<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\ContactUs as Contact;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;
    public $contactus;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contactus)
    {
        $this->contactus = $contactus;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from(config('mail.contact_us.mail'))
            ->subject('El usuario ' . $this->contactus->name . ' se ha contactado')
            ->markdown('mails.contact_us');
    }
}
