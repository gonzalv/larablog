<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str as Str;

use App\Category;
use App\Post;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::select('title', 'slug', 'body')->orderBy('title')->paginate(5);
        return view('categories.index')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:categories|max:255',
            'body' => 'required',
        ]);

        $category = new Category;

        $category->title = $request->title;
        $category->body = $request->body;
        
        $slug = Str::slug($category->title);
        $category->slug = $slug;
        $category->save();

        $request->session()->flash('status', 'New Category: "'. $category->title .'", was created succesfully!');
        return redirect()->route('categories.show', $category->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    { 
        $category = Category::where('slug', $slug)
                                ->select(['title', 'body', 'slug', 'id'])
                                ->first();
        $posts = Post::where('category_id', $category->id)->latest()->paginate(5);

        return view('categories.show')
                    ->with('category', $category)
                    ->with('posts', $posts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $category = Category::where('slug', $slug)
                                ->select(['title', 'body', 'slug'])
                                ->first();

        return view('categories.edit')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $category = Category::where('slug', $slug)->first();

        $this->validate($request, [
            'title' => 'required|max:255|unique:categories,title,' . $category->id,
            'body' => 'required'
        ]);

        $category->title = $request->title;

        $slug = Str::slug($category->title);
        $category->slug = $slug;
        $category->body = $request->body;

        $category->save();

        $request->session()->flash('status', 'The '. $category->title .' category was edited succesfully!');
        return redirect()->route('categories.show', $category->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $category = Category::where('slug', $slug)->first();
        $title = $category->title;

        $posts = Post::where('category_id', $category->id)->get();
        if($posts->all() != null) {
            session()->flash('status', 'The Category: "'. $title .'", cannot be deleted.');
            return redirect()->route('categories.show', $category->slug);
        }

        $category->delete();

        session()->flash('status', 'The Category: "'. $title .'", was deleted.');
        return redirect('/admin/categories');
    }
}
