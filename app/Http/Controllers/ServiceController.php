<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str as Str;

use Storage;
use App\Service;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::select(['name', 'introduction', 'slug'])->orderBy('name')->paginate(10);
        return view('services.index')->with('services', $services);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:services|max:255',
            'introduction' => 'required',
        ]);

        $service = new Service;
        $service->name = $request->name;

        $service->introduction = $request->introduction;

        $slug = Str::slug($service->name);
        $service->slug = $slug;

        $service->save();

        $request->session()->flash('status', 'New Service: "'. $service->name .'", was created succesfully!');
        return redirect()->route('services.show', $service->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $service = Service::where('slug', $slug)->select(['name', 'introduction', 'slug'])->first();
        return view('services.show')->with('service', $service);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $service = Service::where('slug', $slug)->select(['name', 'slug', 'introduction'])->first();

        return view('services.edit')->with('service', $service);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $service = Service::where('slug', $slug)->first();

        $this->validate($request, [
            'name' => 'required|max:255|unique:services,name,' . $service->id,
            'introduction' => 'required',
        ]);

        $service->name = $request->name;
        $service->introduction = $request->introduction;
        
        $slug = Str::slug($service->name);
        $service->slug = $slug;

        $service->save();

        $request->session()->flash('status', 'The Service: "'. $service->name .'", was edited.');
        return redirect()->route('services.show', $service->slug); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $service = Service::where('slug', $slug)->first();   
        $title = $service->name;

        $service->delete();

        session()->flash('status', 'The Service: "'. $title .'", was deleted.');
        return redirect('/admin/services');
    }
}
