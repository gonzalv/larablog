<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactUs;
use App\ContactUs as Contact;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function store (Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'mail' => 'required',
            'comment' => 'required',
        ]);

        $contactus = new Contact;
        $contactus->name = $request->name;
        $contactus->email = $request->mail;
        $contactus->message = $request->comment;

        $contactus->save();

        $contactus = new ContactUs($contactus);

        Mail::to('erick@gendra.mx')->cc('andrade@gendra.mx')->send($contactus);

        return back();
    }
}
