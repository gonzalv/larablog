<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Post;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select(['name', 'id', 'email'])->orderBy('name')->paginate(5);

        return view('users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required',
            'repeat_password' => 'required',
        ]);

        if ($request->password != $request->repeat_password) {
            session()->flash('status', 'Password do not match.');
            return redirect()->route('users.create');
        }

        $user = new User;

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);

        $user->save();

        $request->session()->flash('status', 'New User: "' . $user->name . '", was created succesfully!');
        return redirect()->route('users.show', $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::where('id', $id)->select(['id', 'name', 'email'])->first();
        $posts = Post::where('user_id', $user->id)->latest()->paginate(5);

        return view('users.show')->with('user', $user)->with('posts', $posts);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)->select(['id', 'name', 'email'])->first();
        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id', $id)->first();
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required',
            'repeat_password' => 'required',
            'email' => 'required|unique:users,email,' . $user->id,
        ]);

        if ($request->password != $request->repeat_password) {
            session()->flash('status', 'Password do not match.');
            return redirect()->route('users.edit', $user->id);
        }

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);

        $user->save();

        $request->session()->flash('status', 'User: "' . $user->name . '", was updated!');
        return redirect()->route('users.show', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('id', $id)->first();
        $name = $user->name;

        $posts = Post::where('user_id', $user->id)->get();
        if ($posts->all() != null) {
            session()->flash('status', 'The User: "' . $name . '", cannot be deleted.');
            return redirect()->route('users.show', $user->id);
        }

        $user->delete();

        session()->flash('status', 'The User: "' . $name . '", was deleted.');
        return redirect('/admin/users');
    }
}
