<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str as Str;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Tag;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::select(['title', 'body', 'slug', 'created_at', 'category_id', 'user_id'])
            ->with(['category:title,slug','user:name'])
            ->latest()
            ->paginate(5);
        return view('posts.index')->with('posts', $posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::select(['title', 'id'])->orderBy('title')->get();
        $tags = Tag::select(['title', 'id'])->orderBy('title')->get();

        return view('posts.create')
            ->with('categories', $categories)
            ->with('tags', $tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:posts|max:255',
            'body' => 'required',
            'category_id' => 'required',
        ]);

        $post = new Post;

        $post->user_id = Auth::id();
        $post->title = $request->title;
        $post->body = $request->body;

        $post->category_id = $request->category_id;

        $slug = Str::slug($post->title);
        $post->slug = $slug;

        $post->save();

        $post->tags()->attach($request->tags_id);

        $request->session()->flash('status', 'Posted!');
        return redirect()->route('posts.show', $post->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)
            ->with(['user:id,name', 'category:id,slug,title'])
            ->first();
        $user = $post->user;
        $category = $post->category;
        $tags = $post->tags;
        $comments = $post->comments;

        return view('posts.show')
            ->with('post', $post)
            ->with('user', $user)
            ->with('category', $category)
            ->with('tags', $tags)
            ->with('comments', $comments);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $post = Post::where('slug', $slug)
            ->select(['title', 'body', 'slug'])
            ->first();
        $categories = Category::select(['title', 'id'])->orderBy('title')->get();
        $tags = Tag::select(['title', 'id'])->orderBy('title')->get();

        return view('posts.edit')
            ->with('post', $post)
            ->with('tags', $tags)
            ->with('categories', $categories);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $post = Post::where('slug', $slug)->first();

        $this->validate($request, [
            'title' => 'required|max:255|unique:posts,title,' . $post->id,
            'body' => 'required',
            'category_id' => 'required',
        ]);

        $post->title = $request->title;
        $post->body = $request->body;
        $post->category_id = $request->category_id;

        $slug = Str::slug($post->title);
        $post->slug = $slug;

        $post->save();
        $post->tags()->sync($request->tags_id);

        $request->session()->flash('status', 'Post edited.');
        return redirect()->route('posts.show', $post->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $post = Post::where('slug', $slug)->first();
        $tags = $post->tags;
        $post->tags()->detach($tags);
        $title = $post->title;
        $post->delete();

        session()->flash('status', 'The Post: "' . $title . '", was deleted successfully!');
        return redirect('/admin/posts');
    }
}
