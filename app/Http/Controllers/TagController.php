<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Str as Str;

use App\Tag;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = Tag::select('title', 'slug')->orderBy('title')->paginate(5);

        return view('tags.index')->with('tags', $tags);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:tags|max:255',
        ]);

        $tag = new Tag;
        $tag->title = $request->title;

        $slug = Str::slug($tag->title);
        $tag->slug = $slug;
        $tag->save();

        $request->session()->flash('status', 'The Tag: "' . $tag->title . '", was created succesfully!');
        return redirect()->route('tags.show', $tag->slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug, Request $request)
    {
        $tag = Tag::where('slug', $slug)->select(['title', 'slug', 'id'])->first();
        $posts = $tag->posts->sortByDesc('created_at')->all();
        $total_posts = count($posts);
        $perPage = 5;
        $offSetPages = $request->input('page', 1) - 1;

        $posts = array_slice($posts, $offSetPages * $perPage, $perPage);
        $posts = new LengthAwarePaginator($posts, $total_posts, $perPage , $request->page, ['path'=>url('/admin/tags/' . $tag->slug)]);

        return view('tags.show')
            ->with('posts', $posts)
            ->with('tag', $tag);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $tag = Tag::where('slug', $slug)->select(['title', 'slug'])->first();

        return view('tags.edit')->with('tag', $tag);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $tag = Tag::where('slug', $slug)->first();

        $this->validate($request, [
            'title' => 'required|max:255|unique:tags,title,' . $tag->id,
        ]);

        $tag->title = $request->title;

        $slug = Str::slug($request->title);
        $tag->slug = $slug;

        $tag->save();

        $request->session()->flash('status', 'The tag: "' . $tag->title . '", was updated.');
        return redirect()->route('tags.show', $tag->slug);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {
        $tag = Tag::where('slug', $slug)->first();
        $posts = $tag->posts;
        $tag->posts()->detach($posts);
        $title = $tag->title;
        $tag->delete();

        session()->flash('session', 'The Tag: "' . $title . '", was deleted.');
        return redirect('/admin/tags');
    }
}
