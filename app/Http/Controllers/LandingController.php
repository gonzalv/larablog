<?php

namespace App\Http\Controllers;

use App\Service;
use App\Post;

class LandingController extends Controller
{
    public function index()
    {
        $services = Service::orderBy('updated_at')->get();
        $posts = Post::latest()->take(2)->get();
        return view('landing')
            ->with('services', $services)
            ->with('posts', $posts);
    }

    public function posts()
    {
        $posts = Post::select(['title', 'body', 'slug', 'created_at', 'category_id', 'user_id'])
            ->with(['category:title,slug','user:name'])
            ->latest()
            ->paginate(5);
        return view('public.posts.index')->with('posts', $posts);
    }

    public function post($slug)
    {
        $post = Post::where('slug', $slug)
            ->with(['user:id,name', 'category:id,slug,title'])
            ->first();
        $user = $post->user;
        $category = $post->category;
        $tags = $post->tags;
        $comments = $post->comments;

        return view('public.posts.show')
            ->with('post', $post)
            ->with('user', $user)
            ->with('category', $category)
            ->with('tags', $tags)
            ->with('comments', $comments);
    }

    public function service($slug)
    {
        $service = Service::where('slug', $slug)->first();
        return view('public.services.show')->with('service', $service);
    }

}
