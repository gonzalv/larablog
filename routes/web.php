<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'LandingController@index');

Route::get('posts/{slug}', 'LandingController@post');
Route::get('posts', 'LandingController@posts');

Route::get('services/{slug}', 'LandingController@service');

Route::post('contact-us', 'ContactUsController@store');

Route::group(['prefix' => 'admin'], function () {

    // Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
	
	Route::group(['middleware' => 'auth'], function (){

		Route::get('/', 'HomeController@index');

		Route::resource('posts', 'PostController');
		Route::resource('comments', 'CommentController');

		Route::resource('categories', 'CategoryController');
		Route::resource('tags', 'TagController');

		Route::resource('services', 'ServiceController');

		Route::resource('users', 'UserController');
	});
});

