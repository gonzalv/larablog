<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str as Str;

use App\Post;
use App\User;
use App\Category;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$some_user = User::all()->first();
    	$some_category = Category::all()->last();
        $some_title = 'Por que los gatos son mejores que los perros?';
        $some_slug = Str::slug($some_title);

        Post::create([
        	'title' => $some_title,
        	'body' => '¿Qué es mejor tener a un gato o a un perro como mascota? Pues la respuesta no los da una vez más la ciencia. Un estudio pionero de 2 mil viejos fósiles reveló que los felinos fueron históricamente los que más sobrevivieron a comparación de los caninos en el pasado.',
        	'user_id' => $some_user->id,
            'slug' => $some_slug,
        ]);

        $another_user = User::all()->last();
    	$another_category = Category::all()->first();
        $another_title = 'Por que los perros son mejores que los gatos?';
        $another_slug = Str::slug($another_title);

        Post::create([
        	'title' => $another_title,
        	'body' => 'Se conoce que la familia de los perros se originó en América del Norte hace uno 40 millones de años y alcanzó un pico de diversidad unos 20 millones de años más tarde, cuando existía más de 30 especies en el continente. Fue entonces que en ese momento los gatos llegaron al continente de Asia.',
        	'user_id' => $another_user->id,
            'slug' => $another_slug,
        ]);
    }
}
