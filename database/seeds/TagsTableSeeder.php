<?php

use Illuminate\Database\Seeder;

use App\Tag;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::create([
            'title' => 'Perros',
            'slug' => 'perros',
        ]);

        Tag::create([
            'title' => 'Gatos',
            'slug' => 'gatos',
        ]);

        Tag::create([
            'title' => 'Alpha',
            'slug' => 'alfa',
        ]);

        Tag::create([
            'title' => 'Omega',
            'slug' => 'omega',
        ]);

        Tag::create([
            'title' => 'Testing',
            'slug' => 'testing',
        ]);

        Tag::create([
            'title' => 'Pruebas',
            'slug' => 'pruebas',
        ]);
    }
}
