<?php

use Illuminate\Database\Seeder;

use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
        	'title' => 'Perros',
        	'body' => 'Aquí se hablará solo de perros.',
            'slug' => 'perros',
        ]);
    
        Category::create([
            'title' => 'Gatos',
            'body' => 'Aquí se hablará solo de gatos.',
            'slug' => 'gatos',
        ]);

        Category::create([
            'title' => 'Alpha',
            'body' => 'Alpha category.',
            'slug' => 'alfa',
        ]);

        Category::create([
            'title' => 'Omega',
            'body' => 'Omega category.',
            'slug' => 'omega',
        ]);

        Category::create([
            'title' => 'Testing',
            'body' => 'For testing.',
            'slug' => 'testing',
        ]);

        Category::create([
            'title' => 'Pruebas',
            'body' => 'Para pruebas.',
            'slug' => 'pruebas',
        ]);
    }
}
