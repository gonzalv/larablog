<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@mail.com',
            'isAdmin' => true,
            'password' => bcrypt(123456),
        ]);

        User::create([
        	'name' => 'User',
        	'email' => 'user@mail.com',
        	'password' => bcrypt(123456),
        ]);
    }
}
