@extends('layouts.public')

@section('title', 'Gendra')

@section('services')
    @foreach ($services as $service)
        <section class="offer--row flex flex-col justify-center align-middle flex-wrap">
            <div class="p-2 flex items-center justify-center flex-row"><span
                        class="text-5xl text-grey-darkest font-gothic">{{ $service->name }}</span> <span
                        class="triangle--brandLogo"></span></div>
            <p class="break-words text-justify mx-4 font-robotoC">{{ $service->introduction }}</p>
            <div class="flex justify-center">
                <a href="/services/{{ $service->slug }}">
                    <button class="gradient-primary m-6 text-white font-lato px-8 py-2 rounded-full w-auto text-lg">
                        Ver más
                    </button>
                </a>
            </div>
        </section>
    @endforeach
@endsection

@section('posts')
    @foreach($posts as $post)
        <article class="blog--entry">
            <section class="bg-white font-roboto"><h2 class="text-gendra-primary">{{ $post->title }}</h2>
                <p class="text-justify mt-3">{!! str_limit($post->body, 250, '...') !!}</p>
                <div class="flex justify-center">
                    <a href="/posts/{{ $post->slug }}">
                    <button class="whitespace-no-wrap gradient-primary m-6 text-white font-lato px-8 py-2 rounded-full w-auto text-lg">
                        Ver más
                    </button>
                    </a>
                </div>
            </section>
        </article>
    @endforeach
@endsection
