@extends('layouts.main')

@section('title', 'Create New Category')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Create Category Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <h4 class="card-title">Create New Category:</h4>
                </div>

                <div class="card-block card-bordered">
                    <div class="card-body">
                        <form method="POST" action="/admin/categories">
                            @csrf
                            <div class="form-group">
                                <label class="form-control-label" for="name-form">Name:</label>
                                <input type="text" name="title" class="form-control" id="name-form">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label card-text" for="body-form">Description:</label>
                                <input type="text" name="body" class="form-control" id="body-form">
                            </div>

                            <div class="form-group">
                                <input value="Create Category" type="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- End Create Category Card -->

        </div>
    </div>
@endsection