@extends('layouts.main')

@section('title', 'Categories')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Categories Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-3">
                            <h4 class="card-title">Categories</h4>
                        </div>
                        <div class="col-sm-8"></div>
                        <div class="col-sm-1">
                            <a class="btn btn-success pull-right" href="/admin/categories/create">
                                <i class="fa fa-plus"></i> New Category
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table ">

                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Options</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{ $category->title }}</td>
                                        <td>{{ str_limit($category->body,100,'...') }}</td>
                                        <td>
                                            <form method="POST" action="/admin/categories/{{ $category->slug }}">
                                                @method('delete')
                                                @csrf
                                                <input type="submit" value="DELETE" class="btn btn-danger pull-right">
                                            </form>
                                            <a class="btn btn-info"
                                               href="/admin/categories/{{ $category->slug }}">Info</a>
                                            <a class="btn btn-success"
                                               href="/admin/categories/{{ $category->slug }}/edit">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <center>
                            {{ $categories->links() }}
                        </center>
                    </div>
                </div>
            </div>
            <!-- End Categories Card -->

        </div>
    </div>
@endsection