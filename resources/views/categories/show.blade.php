@extends('layouts.main')

@section('title', $category->title)

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Category Info Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9">
                                <h1 class="card-title">Category: {{ $category->title }}</h1>
                            </div>
                            <div class="col-md-1">
                                <a href="/admin/categories/{{ $category->slug }}/edit" class="btn btn-primary">Edit</a>
                            </div>
                            <div class="col-md-2">
                                <form method="POST" action="/admin/categories/{{ $category->slug }}">
                                    @method('delete')
                                    @csrf

                                    <input type="submit" value="Delete Category" class="btn btn-danger pull-right">
                                </form>
                            </div>
                        </div>
                    </div>



                </div>

                <div class="card-bordered card-block">
                    <div class="card-body">
                        <p class="card-text">{{ $category->body }}</p>
                    </div>
                </div>

            </div>
            <!-- End Category Info Card -->

            <!-- Begin Related Posts Card -->
            @include('posts.list')
            <!-- End Related Posts Card -->

        </div>
    </div>
@endsection