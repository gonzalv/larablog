@extends('layouts.main')

@section('title')
    Edit | {{ $category->title }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Edit Category Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <h1 class="card-title">Edit Category</h1>
                </div>

                <div class="card-block card-bordered">
                    <div class="card-body">

                        <form method="POST" action="/admin/categories/{{ $category->slug }}">
                            @method('put')
                            @csrf

                            <div class="form-group">
                                <label class="form-control-label" for="title-form">Name:</label>
                                <input type="text" name="title" id="title-form" class="form-control"
                                       value="{{ $category->title }}">
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="body-form">Description:</label>
                                <input type="text" name="body" class="form-control" id="body-form"
                                       value="{{ $category->body }}">
                            </div>


                            <div class="form-group">
                                <input type="submit" value="Update" class="btn btn-success">
                            </div>
                        </form>

                    </div>
                </div>

            </div>
            <!-- End Edit Category Card -->

        </div>
    </div>
@endsection