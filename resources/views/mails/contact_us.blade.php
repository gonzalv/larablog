@component('mail::message')
@component('mail::panel')
<div>
<h3>Un usuario se ha contactado:</h3>
<p>Name: {{ $contactus->name }}</p>
<p>E-mail: {{ $contactus->email }}</p>
<p>Message: {{ $contactus->message }}</p>
</div>
@endcomponent
@component('mail::button', ['url' => 'https://gendra.mx/'])
gendra.mx
@endcomponent
Thanks,<br>
{{ config('app.name') }}
@endcomponent
