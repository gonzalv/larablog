<div class="card box-shadow-1">

    <div class="card-header">
        <di class="row">
            <div class="col-sm-3">
                <h2>Related Posts</h2>
            </div>
            <div class="col-sm-8"></div>
            <div class="col-sm-1">
                @if ($posts[0] === null)
                    <a class="btn btn-small btn-success pull-right" href="/admin/posts/create">
                        <i class="fa fa-plus"></i> New Post
                    </a>
                @endif
            </div>
        </di>
    </div>

    <div class="card-block card-bordered">
        <div class="card-body">

            @foreach($posts as $post)
                <div class="post">
                    <h3>{{ $post->title }}</h3>
                    <p>{!! str_limit($post->body, 500, '...') !!}</p>
                    <a href="/admin/posts/{{ $post->slug }}" class="btn btn-primary">Read More</a>
                </div>
                <hr>
            @endforeach
            <center>
                {{ $posts->links() }}
            </center>
        </div>
    </div>

</div>