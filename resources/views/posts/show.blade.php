@extends('layouts.main')

@section('title')
    {{ $post->title }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Post Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9">
                                <h1 class="card-title">{{ $post->title }}</h1>
                            </div>
                            <div class="col-md-1">
                                <a href="/admin/posts/{{ $post->slug }}/edit" class="btn btn-primary">Edit</a>
                            </div>
                            <div class="col-md-2">
                                <form method="POST" action="/admin/posts/{{ $post->slug }}">
                                    @method('delete')
                                    @csrf

                                    <input type="submit" value="Delete Post" class="btn btn-danger pull-right">
                                </form>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="card-text">
                        <p>Author: {{ $user->name }}.</p>
                        <p>Created: {{ $post->created_at }}</p>
                        <p>Category: <a href="/admin/categories/{{ $category->slug }}">{{ $category->title }}</a></p>
                    </div>
                </div>

                <div class="card-block card-bordered">
                    <div class="card-body">
                        <p class="card-text">{!! $post->body !!}</p>
                    </div>
                </div>

                <div class="card-footer">
                    <p class="card-text">Tags:</p>
                    <div class="container">
                        <div class="row">
                            @foreach ($tags as $tag)
                            <div class="col-xs-1">
                                    <a href="/admin/tags/{{ $tag->slug }}">{{ $tag->title }}</a>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
            <!-- End Post Card -->

            <!-- Begin Comments Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <h4 class="card-title">Comments</h4>
                </div>

                <div class="card-block card-bordered">
                    @foreach ($comments as $comment)
                        <div class="card box-shadow-1">

                            <div class="card-header">
                                <h5>
                                    @if ($comment->user != null)
                                        {{ $comment->user->name }}
                                    @else
                                        Guest
                                    @endif
                                </h5>
                            </div>

                            <div class="card-block card-bordered">
                                <div class="card-body">
                                    <p class="card-text">{!! $comment->body !!}</p>
                                </div>
                            </div>

                        </div>
                    @endforeach

                    <hr>
                    @include('posts.createComment')

                </div>

            </div>
            <!-- End Comments Card -->

        </div>
    </div>
@endsection