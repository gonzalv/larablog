<div class="card box-shadow-1">

    <div class="card-header">
        <h4 class="card-title">Create New Comment:</h4>
    </div>

    <div class="card-body">
        <div class="card-block card-bordered">
            <form method="POST" action="/admin/comments/{{ $post->slug}}">
                @method('put')
                @csrf

                <div class="form-group">
                    <textarea rows="5" cols="50" name="body" class="form-control tinymce"></textarea>
                </div>

                <div class="form-group">
                    <input type="submit" value="Add Comment" class="btn btn-success">
                </div>
            </form>
        </div>
    </div>
</div>