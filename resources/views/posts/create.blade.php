@extends('layouts.main')

@section('title', 'Create New Post')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Create Post Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <h1 class="card-title">Create New Post</h1>
                </div>

                <div class="card-body">
                    <div class="card-block card-bordered">
                        <form method="POST" action="/admin/posts">
                            @csrf

                            <div class="form-group">
                                <label class="form-control-label" for="title-form">Title:</label>
                                <input type="text" name="title" id="title-form" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="body-form">Body:</label>
                                <textarea rows="10" cols="50" name="body" id="body-form"
                                          class="form-control tinymce"></textarea>
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="category-form">Category:</label>
                                <select name="category_id" id="category-form" class="form-control">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">
                                            {{ $category->title }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <p class="card-text">Tags:</p>
                            <div class="container form-group">
                                <div class="row">
                                    @foreach($tags as $tag)
                                        <div class="col-sm-3  col-md-2">
                                            <label class="form-check-label" for="{{ $tag->id }}">
                                                <input class="form-check-input" type="checkbox"
                                                       name="tags_id[]"
                                                       value="{{ $tag->id }}"
                                                       id="{{ $tag->id }}">
                                                {{ $tag->title }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Post It!" class="btn btn-success">
                            </div>

                        </form>
                    </div>
                </div>

            </div>
            <!-- Begin Create Post Card -->

        </div>
    </div>
@endsection