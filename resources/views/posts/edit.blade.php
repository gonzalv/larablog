@extends('layouts.main')

@section('title', $post->title)

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Edit Post Card-->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <h1 class="card-title">Edit Post</h1>
                </div>

                <div class="card-block card-bordered">
                    <div class="card-body">
                        <form method="POST" action="/admin/posts/{{ $post->slug }}">
                            @method('put')
                            @csrf

                            <div class="form-group">
                                <label class="form-control-label" for="title-form">Title:</label>
                                <input id="title-form" type="text" name="title" class="form-control" value="{{ $post->title }}">
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="body-form">Body:</label>
                                <textarea name="body" rows="10" class="form-control tinymce" id="body-form">
                                    {{ $post->body }}
                                </textarea>
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="category-form">Category:</label>
                                <select name="category_id" class="form-control" id="category-form">
                                    @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">
                                            {{ $category->title }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <p class="card-text">Tags:</p>
                            <div class="container form-group">
                                <div class="row">
                                    @foreach($tags as $tag)
                                        <div class="col-sm-3  col-md-2">
                                            <label class="form-check-label" for="{{ $tag->id }}">
                                                <input class="form-check-input" type="checkbox"
                                                       name="tags_id[]"
                                                       value="{{ $tag->id }}"
                                                       id="{{ $tag->id }}">
                                                {{ $tag->title }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Update" class="btn btn-success">
                            </div>

                        </form>

                    </div>
                </div>

            </div>
            <!-- End Edit Post Card-->

        </div>
    </div>
@endsection