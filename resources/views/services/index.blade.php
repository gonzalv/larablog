@extends('layouts.main')

@section('title', 'Services')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Services Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-3">
                            <h4 class="card-title">Services</h4>
                        </div>
                        <div class="col-sm-8"></div>
                        <div class="col-sm-1">
                            <a class="btn btn-success pull-right" href="/admin/services/create">
                                <i class="fa fa-plus"></i> New Service
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table ">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Introduction</th>
                                    <th>Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($services as $service)
                                    <tr>
                                        <td>{{ $service->name }}</td>
                                        <td>{{ str_limit($service->introduction,25,'...') }}</td>
                                        <td>

                                            <form method="POST" action="/admin/services/{{ $service->slug }}">
                                                @method('delete')
                                                @csrf
                                                <input type="submit" value="DELETE" class="btn btn-danger pull-right">
                                            </form>

                                            <a class="btn btn-info" href="/admin/services/{{ $service->slug }}">Info</a>
                                            <a class="btn btn-success" href="/admin/services/{{ $service->slug }}/edit">Edit</a>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                        <center>
                            {{ $services->links() }}
                        </center>

                    </div>
                </div>
            </div>
            <!-- End Services Card -->

        </div>
    </div>
@endsection