@extends('layouts.main')

@section('title', 'Create New Service')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Create Service Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <h1 class="card-title">Create New Service</h1>
                </div>

                <div class="card-body">
                    <div class="card-block card-bordered">
                        <form method="POST" action="/admin/services" enctype="multipart/form-data">
                            @csrf

                            <div class="form-group">
                                <label class="form-control-label" for="name-form">Name:</label>
                                <input type="text" name="name" id="name-form" class="form-control">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label" for="introduction-form">Introduction:</label>
                                <textarea rows="10" cols="50" id="introduction-form" name="introduction" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <input value="Create Service" type="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- End Create Service Card -->

        </div>
    </div>
@endsection