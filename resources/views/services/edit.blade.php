@extends('layouts.main')

@section('title')
    Edit | {{ $service->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Create Service Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <h1 class="card-title">Edit Service</h1>
                </div>

                <div class="card-body">
                    <div class="card-block card-bordered">

                        <form method="POST" action="/admin/services/{{ $service->slug }}">
                            @method('put')
                            @csrf

                            <div class="form-group">
                                <label class="form-control-label" for="name-form">Name:</label>
                                <input type="text" name="name" class="form-control" id="name-form"
                                       value="{{ $service->name }}">
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="introduction-form">Introduction:</label>
                                <textarea name="introduction" rows="10" id="introduction-form" class="form-control">
                                {{ $service->introduction }}
                            </textarea>
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Update" class="btn btn-success">
                            </div>
                        </form>

                    </div>
                </div>

            </div>
            <!-- End Create Service Card -->

        </div>
    </div>
@endsection