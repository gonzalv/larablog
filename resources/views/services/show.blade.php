@extends('layouts.main')

@section('title')
    {{ $service->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Service Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9">
                                <h1 class="card-title">{{ $service->name }}</h1>
                            </div>
                            <div class="col-md-1">
                                <a href="/admin/services/{{ $service->slug }}/edit" class="btn btn-primary pull-right">
                                    Edit
                                </a>
                            </div>
                            <div class="col-md-2">
                                <form method="POST" action="/admin/services/{{ $service->slug }}">
                                    @method('delete')
                                    @csrf

                                    <input type="submit" value="Delete Service" class="btn btn-danger pull-right">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="card-block card-bordered">
                        <div class="row">
                            <div class="col-md">
                                <p class="card-text">{{ $service->introduction }}</p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End Service Card -->



        </div>
    </div>
@endsection