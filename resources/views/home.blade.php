@extends('layouts.main')

@section('title', 'Back Office')

@section('content')
    <div class="row">

        <div class="col-xl-2 col-lg-6 col-xs-12">
            <div class="card box-shadow-1">
                <a href="/admin/posts">
                    <div class="card-block">
                        <div class="card-body">
                            <div class="media">
                                <div class="media-left media-middle">
                                    <i class="ft-book"></i>
                                </div>
                                <div class="media-body text-xs-center">
                                    <p class="card-text">Posts</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-xl-2 col-lg-5 col-xs-12">
            <div class="card box-shadow-1">
                <a href="/admin/services">
                    <div class="card-block">
                        <div class="card-body">
                            <div class="media">
                                <div class="media-left media-middle">
                                    <i class="ft-briefcase"></i>
                                </div>
                                <div class="media-body text-xs-center">
                                    <p class="card-text">Services</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-xl-2 col-lg-5 col-xs-12">
            <div class="card box-shadow-1">
                <a href="/admin/users">
                    <div class="card-block">
                        <div class="card-body">
                            <div class="media">
                                <div class="media-left media-middle">
                                    <i class="ft-user"></i>
                                </div>
                                <div class="media-body text-xs-center">
                                    <p class="card-text">Users</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

    </div>
@endsection