@extends('layouts.main')

@section('title')
    Edit | {{ $tag->title }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Edit Tag Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <h1 class="card-title">Edit Tag</h1>
                </div>

                <div class="card-body">
                    <div class="card-block card-bordered">
                        <form method="POST" action="/admin/tags/{{ $tag->slug }}">
                            @method('put')
                            @csrf

                            <div class="form-group">
                                <label class="form-control-label" for="title-form">Name:</label>
                                <input type="text" name="title" id="title-form" class="form-control" value="{{ $tag->title }}">
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Update" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- End Edit Tag Card -->

        </div>
    </div>
@endsection