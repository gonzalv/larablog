@extends('layouts.main')

@section('title')
    Tag | {{ $tag->title }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Tag Info Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9">
                                <h1 class="card-title">Tag: {{ $tag->title }}</h1>
                            </div>
                            <div class="col-md-1">
                                <a href="/admin/tags/{{ $tag->slug }}/edit" class="btn btn-primary">Edit</a>
                            </div>
                            <div class="col-md-2">
                                <form method="POST" action="/admin/tags/{{ $tag->slug }}">
                                    @method('delete')
                                    @csrf

                                    <input type="submit" value="Delete Category" class="btn btn-danger pull-right">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Tag Info Card -->

            <!-- Begin Related Posts Card -->
        @include('posts.list')
        <!-- End Related Posts Card -->

        </div>
    </div>
@endsection