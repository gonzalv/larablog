@extends('layouts.main')

@section('title', 'Tags')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Tags Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-3">
                            <h1 class="card-title">Tags</h1>
                        </div>
                        <div class="col-sm-8"></div>
                        <div class="col-sm-1">
                            <a class="btn btn-success pull-right" href="/admin/tags/create">
                                <i class="fa fa-plus"></i> New Tag
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="card-block card-bordered">
                        <div class="table-responsive">
                            <table class="table ">

                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Options</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($tags as $tag)
                                    <tr>
                                        <td>{{ $tag->title }}</td>
                                        <td>
                                            <form method="POST" action="/admin/tags/{{ $tag->slug }}">
                                                @method('delete')
                                                @csrf
                                                <input type="submit" value="DELETE" class="btn btn-danger pull-right">
                                            </form>
                                            <a class="btn btn-info"
                                               href="/admin/tags/{{ $tag->slug }}">Info</a>
                                            <a class="btn btn-success"
                                               href="/admin/tags/{{ $tag->slug }}/edit">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <center>
                            {{ $tags->links() }}
                        </center>
                    </div>
                </div>

            </div>
            <!-- End Tags Card -->

        </div>
    </div>
@endsection