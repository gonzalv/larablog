@extends('layouts.main')

@section('title', 'Create New Tag')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Create Tag Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <h1 class="card-title">Create New Tag</h1>
                </div>

                <div class="card-body">
                    <div class="card-block card-bordered">
                        <form method="POST" action="/admin/tags">
                            @csrf

                            <div class="form-group">
                                <label class="form-control-label" for="title-form">Name:</label>
                                <input type="text" name="title" class="form-control" id="title-form">
                            </div>

                            <div class="form-group">
                                <input value="Create Tag" type="submit" class="btn btn-success">
                            </div>
                        </form>
                    </div>
                </div>

            </div>
            <!-- End Create Tag Card -->

        </div>
    </div>
@endsection