<div data-scroll-to-active="true" class="main-menu menu-fixed menu-dark menu-accordion menu-shadow">
    <!-- main menu content-->
    <div class="main-menu-content">
        <ul id="main-menu-navigation" data-menu="menu-navigation" class="navigation navigation-main">

            <li class=" nav-item">
                <a href="/admin">
                    <i class="ft-home"></i>
                    <span data-i18n="" class="menu-title">Home</span>
                </a>
            </li>

            <li class=" nav-item">
                <a href="/admin/posts">
                    <i class="ft-book"></i>
                    <span data-i18n="" class="menu-title">Posts</span>
                </a>
                <ul class="menu-content">

                    <li>
                        <a href="/admin/posts">
                            <i class="ft-list"></i>
                            <span data-i18n="" class="menu-title">List</span>
                        </a>
                    </li>

                    <li>
                        <a href="/admin/categories">
                            <i class="ft-bookmark"></i>
                            <span data-i18n="" class="menu-title">Categories</span>
                        </a>
                    </li>

                    <li>
                        <a href="/admin/tags">
                            <i class="ft-tag"></i>
                            <span data-i18n="" class="menu-title">Tags</span>
                        </a>
                    </li>

                </ul>
            </li>

            <li class=" nav-item">
                <a href="/admin/services">
                    <i class="ft-briefcase"></i>
                    <span data-i18n="" class="menu-title">Services</span>
                </a>
            </li>

            <li>
                <a href="/admin/users">
                    <i class="fa fa-user"></i>
                    <span data-i18n="" class="menu-title">Users</span>
                </a>
            </li>

        </ul>
    </div>
    <!-- /main menu content-->
</div>