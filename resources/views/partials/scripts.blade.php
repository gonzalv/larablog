<!-- BEGIN VENDOR JS-->
<script src="../../../app-assets/js/core/libraries/jquery.min.js" type="text/javascript"></script>
<script src="../../../app-assets/vendors/js/ui/tether.min.js" type="text/javascript"></script>
<script src="../../../app-assets/js/core/libraries/bootstrap.min.js" type="text/javascript"></script>
<script src="../../../app-assets/vendors/js/ui/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="../../../app-assets/vendors/js/ui/unison.min.js" type="text/javascript"></script>
<script src="../../../app-assets/vendors/js/ui/jquery-sliding-menu.js" type="text/javascript"></script>
<!-- BEGIN VENDOR JS-->
<!-- BEGIN PAGE VENDOR JS-->
<!-- END PAGE VENDOR JS-->
<!-- BEGIN STACK JS-->
<script src="../../../app-assets/js/core/app-menu.js" type="text/javascript"></script>
<script src="../../../app-assets/js/core/app.js" type="text/javascript"></script>
<!-- END STACK JS-->
<!-- BEGIN PAGE LEVEL JS-->
<!-- END PAGE LEVEL JS-->