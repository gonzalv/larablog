@if(Session::has('status'))
  <p class="alert alert-info">{{ Session::get('status') }}</p>
@endif
@if ($errors->any())
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
@endif