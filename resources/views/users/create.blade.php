@extends('layouts.main')

@section('title', 'Create New User')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Create User Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <h4 class="card-title">Create New User</h4>
                </div>

                <div class="card-block card-bordered">
                    <div class="card-body">
                        <form method="POST" action="/admin/users">
                            @csrf

                            <div class="form-group">
                                <label for="name-form" class="form-control-label">Name:</label>
                                <input id="name-form" type="text" name="name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="email-form" class="form-control-label">E-mail:</label>
                                <input id="email-form" type="email" name="email" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="password-form" class="form-control-label card-text">Password:</label>
                                <input id="password-form" type="password" name="password" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="repeat-password-form" class="form-control-label card-text">Repeat Password:</label>
                                <input id="repeat-password-form" type="password" name="repeat_password" class="form-control">
                            </div>

                            <div class="form-group">
                                <input value="Create User" type="submit" class="btn btn-success">
                            </div>

                        </form>
                    </div>
                </div>

            </div>
            <!-- End Create User Card -->

        </div>
    </div>
@endsection