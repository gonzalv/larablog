@extends('layouts.main')

@section('title')
    Edit | {{ $user->name}}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Edit User Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <h1 class="card-title">Edit User</h1>
                </div>

                <div class="card-block card-bordered">
                    <div class="card-body">

                        <form method="POST" action="/admin/users/{{ $user->id}}">
                            @method('put')
                            @csrf

                            <div class="form-group">
                                <label class="form-control-label" for="name-form">Name:</label>
                                <input type="text" name="name" id="name-form" class="form-control" value="{{ $user->name}}">
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="email-form">E-mail:</label>
                                <input type="email" name="email" id="email-form" class="form-control" value="{{ $user->email}}">
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="password-form">Password:</label>
                                <input type="password" name="password" id="password-form" class="form-control">
                            </div>

                            <div class="form-group">
                                <label class="form-control-label" for="repeat-password-form">Repeat Password:</label>
                                <input type="password" id="repeat-password-form" name="repeat_password" class="form-control">
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Update" class="btn btn-success">
                            </div>

                        </form>

                    </div>
                </div>

            </div>
            <!-- End Edit User Card -->

        </div>
    </div>
@endsection