@extends('layouts.main')

@section('title', 'Users')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin Users Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <div class="row">
                        <div class="col-sm-3">
                            <h4 class="card-title">Users</h4>
                        </div>
                        <div class="col-sm-8"></div>
                        <div class="col-sm-1">
                            <a class="btn btn-success pull-right" href="/admin/users/create">
                                <i class="fa fa-plus"></i> New User
                            </a>
                        </div>
                    </div>
                </div>

                <div class="card-content">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table ">

                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>E-mail</th>
                                    <th>Password</th>
                                    <th>Options</th>
                                </tr>
                                </thead>

                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $user->name}}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>Secret</td>
                                        <td>
                                            <form method="POST" action="/admin/users/{{ $user->id }}">
                                                @method('delete')
                                                @csrf
                                                <input type="submit" value="DELETE" class="btn btn-danger pull-right">
                                            </form>
                                            <a class="btn btn-info"
                                               href="/admin/users/{{ $user->id}}">Info</a>
                                            <a class="btn btn-success"
                                               href="/admin/users/{{ $user->id}}/edit">Edit</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                        </div>
                        <center>
                            {{ $users->links() }}
                        </center>
                    </div>
                </div>
            </div>
            <!-- End Users Card -->

        </div>
    </div>
@endsection