@extends('layouts.main')

@section('title', $user->name)

@section('content')
    <div class="row">
        <div class="col-md-12">

            <!-- Begin User Info Card -->
            <div class="card box-shadow-1">

                <div class="card-header">
                    <h1 class="card-title">Name: {{ $user->name }}</h1>
                </div>

                <div class="card-bordered card-block">
                    <div class="card-body">
                        <p class="card-text">E-mail: {{ $user->email }}</p>
                        <p class="card-text">Password: Secret</p>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2">
                                    <a href="/admin/users/{{ $user->id}}/edit" class="btn btn-primary">Edit</a>
                                </div>
                                <div class="col-md-8"></div>
                                <div class="col-md-2">
                                    <form method="POST" action="/admin/users/{{ $user->id }}">
                                        @method('delete')
                                        @csrf
                                        <input type="submit" value="DELETE" class="btn btn-danger pull-right">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- End User Info Card -->

            <!-- Begin Related Posts Card -->
            @include('posts.list')
            <!-- End Related Posts Card -->

        </div>
    </div>
@endsection