<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="/favicon.b53ccc31.ico" type="image/x-icon">
    <link rel="icon" sizes="96x96" href="/96x96.bfa6e1b0.svg">
    <link rel="icon" sizes="48x48" href="/48x48.82906924.svg">
    <meta name="theme-color" content="#273251">
    <link rel="manifest" href="/manifest.4c26097b.js">
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBAIMfgc4BZTcjHjBrgi60I-1OHZ-fV2J0"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.25/gmaps.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Roboto+Condensed:300|Lato|Roboto"
          rel="stylesheet">
    <title>Gendra</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="/entry.e7048840.css">
</head>
<body>
<main>
    <div id="gendra--header" class="flex flex-col flex-no-wrap w-full h-screen">
        <header id="gendra--topbar" class="flex justify-between md:justify-between"><a href="/"
                                                                                       class="ml-8 m-2 p-2 text-white no-underline font-gothic flex items-center justify-center flex-row">
                <span class="text-5xl">gendra</span> <span class="triangle--brandLogo"></span> </a>
            <nav id="gendra--menu" class="flex w-full justify-start items-center m-2">
                <ul class="list-reset flex flex-row justify-center uppercase font-robotoC">
                    <li class="p-2"><a href="#" class="no-underline text-white">inicio</a></li>
                    <li class="p-2"><a href="#gendra--offer" class="no-underline text-white">Servicios</a></li>
                    <li class="p-2"><a href="#gendra--success" class="no-underline text-white">Portafolio</a></li>
                    <li class="p-2"><a href="#gendra--blog" class="no-underline text-white">Blog</a></li>
                    <li class="p-2"><a href="#gendra--contactus" class="no-underline text-white">Contacto</a></li>
                </ul>
            </nav>
            <div class="flex items-center">
                <button id="gendra--menu--contact"
                        class="gradient-secondary shadow-md mr-6 rounded-full py-2 px-4 font-robotoC font-bold text-white">
                    Contáctanos
                </button>
                <button id="gendra--menu--icon" class="text-xl mr-6 bg-transparent text-white m-2"><i
                            class="material-icon">menu</i></button>
            </div>
        </header>
        <article class="flex items-center justify-start h-full mx-8"><h1 class="text-white font-roboto big--header">
                Desarrollo de software avanzado para empresas</h1></article>
    </div>
    <article id="gendra--offer" class="w-full flex justify-start">
        <div class="flex align-baseline flex-row flex-no-wrap justify-center">
            <div class="vertical--line mx-4"></div>
            <div id="gendra--offer--content" class="my-8"><h1
                        class="text-gendra-secondary title--header font-roboto my-8 mx-3">Descubre nuestra oferta de
                    software</h1>
                <div class="gendra--offers flex flex-row flex-wrap justify-center">

                    @yield('services')

                </div>
            </div>
        </div>
    </article>
    <article id="gendra--process" class="bg-white w-full flex justify-start">
        <div class="flex align-baseline justify-center flex-row flex-no-wrap w-full">
            <div class="vertical--line mx-4"></div>
            <article class="my-8 w-full"><h1 class="text-gendra-primary title--header mx-3">Nuestro proceso
                    creativo</h1>
                <section class="my-6 flex flex-row flex-wrap">
                    <div class="gendra--process--row">
                        <object width="300" height="300" data="/investigacion.f7934c47.svg"
                                type="image/svg+xml"></object>
                        <h3>Investigación</h3></div>
                    <div class="gendra--process--row">
                        <object width="300" height="300" data="/analisis.5c5c7e03.svg" type="image/svg+xml"></object>
                        <h3>Análisis</h3></div>
                    <div class="gendra--process--row">
                        <object width="300" height="300" data="/diseno.78412183.svg" type="image/svg+xml"></object>
                        <h3>Diseño</h3></div>
                    <div class="gendra--process--row">
                        <object width="300" height="300" data="/proto.f0356dec.svg" type="image/svg+xml"></object>
                        <h3>Prototipado</h3></div>
                    <div class="gendra--process--row">
                        <object width="300" height="300" data="/pruebas.881d20b6.svg" type="image/svg+xml"></object>
                        <h3>Pruebas</h3></div>
                    <div class="gendra--process--row">
                        <object width="300" height="300" data="/programacion.919fbe0a.svg"
                                type="image/svg+xml"></object>
                        <h3>Programación</h3></div>
                    <div class="gendra--process--row">
                        <object width="300" height="300" data="/entrega.538d84d2.svg" type="image/svg+xml"></object>
                        <h3>Entregas</h3></div>
                </section>
            </article>
        </div>
    </article>
    <div class="change--line--direction flex flex-row flex-no-wrap bg-white"><span
                class="line--in ml-4 self-start rounded-bl-lg"></span> <span
                class="line--out mr-4 self-end rounded-tr-lg"></span></div>
    <article id="gendra--success" class=" w-full bg-gendra-primary text-white flex justify-end">
        <div class="flex flex-row flex-no-wrap justify-center max-w-5xl">
            <section class="success--content flex flex-row my-8 flex-no-wrap items-center justify-center"><h1
                        class="m-6 title--header"> Conoce nuestros casos de éxito </h1>
                <div id="brands--success" class="flex flex-row justify-center items-center flex-wrap m-2">
                    <object data="/exito5.2457d1a2.svg" type="image/svg+xml"></object>
                    <object data="/exito4.921914e1.svg" type="image/svg+xml"></object>
                    <object data="/exito3.4b4304e2.svg" type="image/svg+xml"></object>
                    <object data="/exito2.d391d7bd.svg" type="image/svg+xml"></object>
                    <object data="/exito1.b77e4350.svg" type="image/svg+xml"></object>
                </div>
            </section>
            <div class="vertical--line mx-4"></div>
        </div>
    </article>
    <article id="gendra--blog" class="w-full flex justify-end">
        <div class="flex flex-row flex-no-wrap justify-end max-w-5xl">
            <section id="gendra--articles" class="my-8 flex flex-col flex-no-wrap items-end text-right"><h1
                        class="text-gendra-secondary m-6 title--header">Lee algo de nuestro blog</h1>
                <div class="flex flex-row justify-end items-center flex-wrap">

                    @yield('posts')

                </div>
            </section>
            <div class="vertical--line mx-4"></div>
        </div>
    </article>
    <button id="gendra--chat--button"
            class="gradient-secondary fixed pin-r pin-b m-6 rounded-full h-16 w-16 text-white leading-normal shadow-lg">
        <i class="material-icon">message</i></button>
    <footer id="gendra--footer" class="relative">
        <div class="vertical--line--end mx-4 absolute"></div>
        <div id="gendra--footer--content" class="absolute bg-gendra-grey text-white">
            <article class="flex flex-wrap flex-row w-full">
                <section class="gendra--footer--part flex flex-col w-full flex-no-wrap items-start" id="gendra--contactus">
                    <div class="flex flex-row flex-no-wrap items-center justify-start">
                        <div class="m-2 p-2 text-white no-underline font-gothic flex items-center justify-center flex-row">
                            <h1 class="text-5xl">g</h1> <span class="triangle--brandLogo"></span></div>
                        <h2>Platiquemos</h2></div>
                    <div class="w-full">
                        <form class="flex flex-col" method="post" action="/contact-us">
                            @csrf

                            <input class="gendra--input" placeholder="TU NOMBRE" required="" type="text" name="name"
                                   id="contact-name"> <input class="gendra--input" type="email" placeholder="TU EMAIL"
                                                             required="" name="mail" id="contact-mail"> <textarea
                                    class="gendra--input" name="comment" placeholder="CUÉNTANOS" required="" cols="20"
                                    rows="2"></textarea> <input type="submit"
                                                                class="gradient-secondary shadow-md m-6 mt-0 rounded-full py-2 px-4 font-robotoC font-bold text-white"
                                                                value="Enviar"></form>
                    </div>
                </section>
                <section
                        class="flex p-6 flex-col text-left justify-start items-start gendra--footer--part bg-gendra-primary">
                    <div class="flex items-center justify-center"><i class="material-icon m-2 text-5xl">phone</i> (55)
                        8436 0392
                    </div>
                    <div class="flex items-center justify-center"><i class="material-icon m-2 text-5xl">chat_bubble</i>
                        hola@gendra.mx
                    </div>
                    <div class="flex items-center justify-center"><i class="material-icon m-2 text-5xl">place</i> Av.
                        Cuauhtémoc 1128, Letrán Valle, Benito Juárez, CDMX
                    </div>
                </section>
            </article>
        </div>
        <div id="gendra--footer--gmap" class="relative"></div>
        <div id="gendra--footer--gmap--overlay" class="absolute"></div>
        <div id="gendra--footer--legend" class="flex items-end justify-center">
            <div class="flex flex-col justify-center items-center p-2">
                <div class="m-2 p-2 text-gendra-primary no-underline font-gothic flex items-center justify-center flex-row">
                    <span class="text-4xl">gendra</span> <span class="triangle--brandLogo"></span></div>
                <span style="margin-top:-20px" class="text-sm">© 2018 Gendra</span></div>
        </div>
    </footer>
</main>
<script defer="" src="/entry.6b613494.js"></script>
</body>
</html>