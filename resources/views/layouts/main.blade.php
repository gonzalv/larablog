<!DOCTYPE html>
<html lang="en" data-textdirection="ltr" class="loading">
<head>
    @include('partials.head')
</head>
<body data-open="click" data-menu="vertical-menu" data-col="2-columns"
      class="vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar">

<!-- navbar-fixed-top-->
@include('partials.navbar')
<!-- / navbar-fixed-top-->


<!-- main menu-->
@include('partials.mainmenu')
<!-- / main menu-->

<!-- page content -->
<div class="app-content content container-fluid">
    <div class="content-wrapper">
        @include('partials.alertBox')
        <div class="content-body">
            @yield('content')
        </div>
    </div>
</div>
<!-- / page content -->

@include('partials.scripts')

</body>
</html>